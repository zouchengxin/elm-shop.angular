import { Component, OnInit, AfterContentChecked } from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-ratings',
  templateUrl: './ratings.component.html',
  styleUrls: ['./ratings.component.css']
})
export class RatingsComponent implements OnInit, AfterContentChecked {
  ratings = [];
  seller = {};
  ratingsArr = [];
  satRatings = [];
  NotSatRatings = [];
  type = 0; // 0-全部 1-满意 2-不满意
  isChecked = true; // 是否选中只看有内容的评论
  constructor(private ds: DataService) { }

  ngOnInit() {
    this.ratings = this.ds.data.ratings;
    this.seller = this.ds.data.seller;
    this.satRatings = this.ratings.filter((v) =>  v.rateType === 0 ? true : false);
    this.NotSatRatings = this.ratings.filter((v) =>  v.rateType === 1 ? true : false);
  }

  onChecked() {
    this.isChecked = !this.isChecked;
  }
  onSatisfy(type, event) {
    this.type = type;
    const cs = event.target.parentNode.children;
    for (let i = 0; i < cs.length; i++) {
      cs[i].classList.remove('active');
    }
    event.target.classList.add('active');
  }

  ngAfterContentChecked(): void {
    switch (this.type) {
      case 1: this.ratingsArr = this.satRatings; break;
      case 2: this.ratingsArr = this.NotSatRatings; break;
      default: this.ratingsArr = this.ratings; break;
    }
  }
}
