import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';

interface Message {
  price: number;
  clientX: number;
  clientY: number;
}
@Injectable({
  providedIn: 'root'
})
export class DataService {
  data = {
    goods: [],
    seller:  {},
    ratings: []
  };
  sPrice = new Subject<Message>();
  constructor(private http: HttpClient) {}
  // @ts-ignore
  getData(): Observable<HttpResponse> {
    return this.http.get('/data.json');
  }
}
