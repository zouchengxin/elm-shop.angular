import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-image-show',
  templateUrl: './image-show.component.html',
  styleUrls: ['./image-show.component.css']
})
export class ImageShowComponent implements OnInit {
  pics = [];
  activeIndex = 0;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParamMap.subscribe((o: any) => {
      this.activeIndex = parseInt(o.params.index, 10);
      this.pics = o.params.pics;
    });
  }

  onImageClick() {
    history.back();
  }

  onNext() {
    this.activeIndex ++;
    if (this.activeIndex > this.pics.length - 1) {
      this.activeIndex = this.pics.length - 1;
    }
  }

  onPrev() {
    this.activeIndex --;
    if (this.activeIndex < 0) {
      this.activeIndex = 0;
    }
  }
}
