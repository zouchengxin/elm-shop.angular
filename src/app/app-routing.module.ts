import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {GoodsComponent} from './goods/goods.component';
import {RatingsComponent} from './ratings/ratings.component';
import {SellersComponent} from './sellers/sellers.component';
import {MainComponent} from './main/main.component';
import {SellerDetailComponent} from './seller-detail/seller-detail.component';
import {StarsComponent} from './stars/stars.component';
import {ImageShowComponent} from './image-show/image-show.component';
import {GoodDetailComponent} from './good-detail/good-detail.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'goods',
        component: GoodsComponent,
        children: [
        ]
      },
      {path: 'sellers', component: SellersComponent},
      {path: 'ratings', component: RatingsComponent},
      {path: 'goodDetail', component: GoodDetailComponent}
    ]
  },
  {
    path: 'sellerDetail',
    component: SellerDetailComponent
  },
  {
    path: 'stars',
    component: StarsComponent
  },
  {
    path: 'image',
    component: ImageShowComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false,
    anchorScrolling: 'enabled',
    urlUpdateStrategy: 'eager',
    scrollPositionRestoration: 'top'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
