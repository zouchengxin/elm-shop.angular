import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sellers',
  templateUrl: './sellers.component.html',
  styleUrls: ['./sellers.component.css'],
})
export class SellersComponent implements OnInit {
  seller: any = {};
  isFavorite = false;
  constructor(private ds: DataService, private router: Router) { }

  ngOnInit() {
    this.seller = this.ds.data.seller;
  }

  onFavoraite() {
    this.isFavorite = ! this.isFavorite;
  }

  onImageShow(i) {
    this.router.navigate(['/image'], {
      skipLocationChange: true,
      queryParams: {
        index : i,
        pics : this.seller.pics
      }
    });
  }
}
