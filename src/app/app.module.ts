import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ReactiveFormsModule} from '@angular/forms';

import {HttpClientModule} from '@angular/common/http';
import { GoodsComponent } from './goods/goods.component';
import { RatingsComponent } from './ratings/ratings.component';
import { SellersComponent } from './sellers/sellers.component';
import { MainComponent } from './main/main.component';
import { SellerDetailComponent } from './seller-detail/seller-detail.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { StarsComponent } from './stars/stars.component';
import { ImageShowComponent } from './image-show/image-show.component';
import { GoodDetailComponent } from './good-detail/good-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    GoodsComponent,
    RatingsComponent,
    SellersComponent,
    MainComponent,
    SellerDetailComponent,
    StarsComponent,
    ImageShowComponent,
    GoodDetailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
