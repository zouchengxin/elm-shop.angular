import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-seller-detail',
  templateUrl: './seller-detail.component.html',
  styleUrls: ['./seller-detail.component.css']
})
export class SellerDetailComponent implements OnInit {
  seller: any = {};
  constructor(private ds: DataService) {
    this.seller = ds.data.seller;
  }
  ngOnInit() {
  }

  onClose() {
    history.back();
  }
}
