import {Component, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../data.service';
import {Router} from '@angular/router';
import {slideInAnimation} from '../animations/slideInAnimation';

@Component({
  selector: 'app-main',
  animations: [
    slideInAnimation
  ],
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  seller: any = {
    name: '',
    description: '',
    deliveryTime: 0,
    score: 0,
    avatar: '',
    supports: [
      {description: ''}
    ],
    bulletin: '',
    serviceScore: 0,
  };
  totalPrice = 0;
  needPrice = 0;
  canAccount = false;
  cartNum = 0;
  tab = true;
  @ViewChild('cart', {static: false}) cart;
  @ViewChild('roundspot', {static: false}) roundspot;
  constructor(private ds: DataService, private router: Router) {}
  ngOnInit(): void {
    this.ds.getData().subscribe((x) => {
      this.seller = x.seller;
      this.needPrice = x.seller.minPrice;
      this.ds.data = x;
      this.router.navigate(['/goods'], {skipLocationChange: false});
    });
    this.ds.sPrice.subscribe((obj) => {
      // console.log(obj);
      this.totalPrice += obj.price;
      this.needPrice -= obj.price;
      if (this.needPrice <= 0) {
        this.canAccount = true;
      } else {
        this.canAccount = false;
      }
      if (this.needPrice > this.seller.minPrice) {
        this.needPrice = this.seller.minPrice;
      }
      let keyframes;
      if (obj.price > 0) {
        this.cartNum ++;
        keyframes = [
          { top: obj.clientY + 'px', left: obj.clientX - 30 + 'px', visibility: 'visible' },
          { top: 670 + 'px', left: 55 + 'px', visibility: 'hidden'},
        ];
      } else {
        this.cartNum --;
        keyframes = [
          { top: 670 + 'px', left: 55 + 'px', visibility: 'visible'},
          { top: obj.clientY + 'px', left: obj.clientX - 30 + 'px', visibility: 'hidden' },
        ];
      }
      if (this.cartNum > 0) {
        this.cart.nativeElement.querySelector('.icon-gouwuche').style.color = '#00a0dc';
      } else {
        this.cart.nativeElement.querySelector('.icon-gouwuche').style.color = 'white';
      }
      this.roundspot.nativeElement.animate(keyframes, {
        fill: 'forwards',
        easing: 'ease-in',
        duration: 500
      }).play();
    });
  }
}
