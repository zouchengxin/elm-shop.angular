import {AfterViewInit, AfterViewChecked,AfterContentInit, Component, Input, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.css']
})
export class StarsComponent implements OnInit, AfterContentInit {
  @Input() ratio ? = 2;
  @Input() total ? = 5;
  @Input() fontSize ? = 12;
  @Input() onColor ? = 'yellow';
  @Input() offColor ? = '#DDDDFF';
  arr = [...Array(this.total)];
  @Input() height ? = 20;
  @Input() width ? = 100;
  part = 0;
  activeWidth = 0;
  @ViewChild('container', {static: false}) container;
  constructor() {
  }
  ngAfterContentInit(): void {
    this.part = this.width / this.total;
    this.activeWidth = (this.ratio / this.total) * this.fontSize * this.total
      + (Math.floor(this.ratio) * 2 + 1) * (this.part - this.fontSize) / 2;
  }
  ngOnInit() {
  }

}
